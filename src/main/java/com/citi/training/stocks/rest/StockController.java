package com.citi.training.stocks.rest;

import static net.logstash.logback.argument.StructuredArguments.keyValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.stocks.dao.StockDao;
import com.citi.training.stocks.model.Stock;

/**
 * This is the REST interface for managing Stock records.
 * 
 * <p>This class adheres to RESTful paths and response codes.</p>
 * 
 * <a href="www.google.com">A Link</a>
 * 
 * @author Frank
 * 
 * @see Stock
 */
@RestController
@RequestMapping("/stocks")
public class StockController {

    private static final Logger LOG = LoggerFactory.getLogger(StockController.class);

    @Autowired
    private StockDao stockDao;

    @RequestMapping(method=RequestMethod.GET)
    public Iterable<Stock> findAll() {
        LOG.info("HTTP GET findAll()");
        return stockDao.findAll();
    }

    /**
     * Find a particular stock record by it's numerical id.
     * 
     * @param id The id of the record to find.
     * @return The Stock object that was found.
     */
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Stock findById(@PathVariable long id) {

    	LOG.info("HTTP GET findById({})", keyValue("stock_id", id));
        //LOG.info("HTTP GET findById() id=[" + id + "]");
        return stockDao.findById(id).get();
    }

    @RequestMapping(method=RequestMethod.POST)
    public HttpEntity<Stock> save(@RequestBody Stock stock) {
        LOG.info("HTTP POST save() stock=[" + stock + "]");
        return new ResponseEntity<Stock>(stockDao.save(stock),
        								 HttpStatus.CREATED); 
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(value=HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable long id) {
        LOG.info("HTTP DELETE delete() id=[" + id + "]");
        stockDao.deleteById(id);
    }
}
