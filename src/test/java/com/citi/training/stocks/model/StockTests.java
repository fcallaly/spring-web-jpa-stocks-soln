package com.citi.training.stocks.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class StockTests {

    private int testId = 49;
    private String testName = "NFLX";
    private String testCompanyName = "Netflix Inc.";

    @Test
    public void test_Stock_defaultConstructorAndSetters() {
        Stock testStock = new Stock();

        testStock.setId(testId);
        testStock.setTicker(testName);
        testStock.setCompanyName(testCompanyName);

        assertEquals("Stock id should be equal to testId",
                     testId, testStock.getId());
        assertEquals("Stock name should be equal to testName",
                     testName, testStock.getTicker());
        assertEquals("Stock company name should be equal to testCompanyName",
                     testCompanyName, testStock.getCompanyName());
    }

    @Test
    public void test_Stock_fullConstructor() {
        Stock testStock = new Stock(testId, testName, testCompanyName);

        assertEquals("Stock id should be equal to testId",
                     testId, testStock.getId());
        assertEquals("Stock name should be equal to testName",
                     testName, testStock.getTicker());
        assertEquals("Stock company name should be equal to testCompanyName",
                     testCompanyName, testStock.getCompanyName());
    }

    @Test
    public void test_Stock_toString() {
        Stock testStock = new Stock(testId, testName, testCompanyName);

        assertTrue("toString should contain testId",
                   testStock.toString().contains(Integer.toString(testId)));
        assertTrue("toString should contain testName",
                testStock.toString().contains(testName));
        assertTrue("toString should contain testCompanyName",
                testStock.toString().contains(testCompanyName));
    }

    @Test
    public void test_Stock_equals() {
        Stock firstStock = new Stock(testId, testName, testCompanyName);

        Stock compareStock = new Stock(testId, testName, testCompanyName);
        assertFalse("stocks for test should not be the same object",
                    firstStock == compareStock);
        assertTrue("stocks for test should be found to be equal",
                   firstStock.equals(compareStock));
    }

    @Test
    public void test_Stock_equalsFails() {
        Stock firstStock = new Stock(testId + 1, testName, testCompanyName);
        Stock secondStock = new Stock(testId, testName + "_test", testCompanyName);
        Stock thirdStock = new Stock(testId, testName, testCompanyName + "_test");
        Stock[] createdStocks = {firstStock, secondStock, thirdStock};

        Stock compareStock = new Stock(testId, testName, testCompanyName);

        for(Stock thisStock: createdStocks) {
            assertFalse("stocks for test should not be the same object",
                        thisStock == compareStock);
            assertFalse("stocks for test should not be found to be equal",
                        thisStock.equals(compareStock));
        }
    }
}
