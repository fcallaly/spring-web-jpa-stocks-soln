package com.citi.training.stocks.rest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.stocks.model.Stock;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2")
public class StockControllerIntegrationTests {

    private static final Logger LOG = LoggerFactory.getLogger(
                                            StockControllerIntegrationTests.class);

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getStock_returnsStock() {
        String testTicker = "MSFT";
        String testCompanyName = "Microsoft Corporation";


        // Create a stock with POST
        ResponseEntity<Stock> createStockResponse = restTemplate.postForEntity(
                                            "/stocks",
                                            new Stock(-1, testTicker, testCompanyName),
                                            Stock.class);

        LOG.info("Create Stock response: " + createStockResponse.getBody());
        assertEquals(HttpStatus.CREATED, createStockResponse.getStatusCode());
        assertEquals("created Stock ticker should equal test ticker",
                testTicker, createStockResponse.getBody().getTicker());
        assertEquals("created Stock company name should equal test company name",
                testCompanyName, createStockResponse.getBody().getCompanyName());


        // Find a stock by Id with HTTP GET
        ResponseEntity<Stock> findStockResponse = restTemplate.getForEntity(
                                        "/stocks/" + createStockResponse.getBody().getId(),
                                        Stock.class);

        LOG.info("FindById Response: " + findStockResponse.getBody());
        assertEquals(HttpStatus.OK, findStockResponse.getStatusCode());
        assertEquals("Found Stock ticker should equal test Ticker",
                     testTicker, findStockResponse.getBody().getTicker());
        assertEquals("Found Stock company name should equal test company name",
                     testCompanyName, findStockResponse.getBody().getCompanyName());


        // Delete a stock with HTTP DELETE
        LOG.debug("Attempting to delete created stock");
        restTemplate.delete("/stocks/" + createStockResponse.getBody().getId());


        // Delete non-existing stock
        restTemplate.delete("/stocks/999");
    }
}
